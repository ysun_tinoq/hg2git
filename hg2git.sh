#!/bin/bash
if [ $# -lt 2 ]; then
    echo "Usage: hg2git.sh <teamName> <repoName> [localRepoPath] [remoteServer]"
    exit -1
fi
python -c "import mercurial"
if [ $? -ne 0 ]; then
    echo "Python module mercurial is missing, run the following command to install it"
    echo "$ pip install mercurial "
    exit -2
fi  
team=$1
repo=$2
remoteRepo="$repo-git"

if [ $# -gt 2 ]; then
    localRepo=$3
else
    localRepo=$repo
fi

if [ $# -gt 3 ]; then
    remoteSvr=$4
else
    remoteSvr="tnq.bitbucket.org"
fi
if [ $# -gt 4 ]; then
    prjRoot=$5
else
    prjRoot='..'
fi
authorMap=${repo}_authors
gitUrl="git@$remoteSvr:$team/${remoteRepo}.git"

echo "About to migrate $repo to $gitUrl, via $prjRoot/$localRepo"

echo "1. Please log in your bitbucket account and create $gitUrl. Once done,"
read -p "hit any key to continue OR Ctrl-C to quit ..." -n1 -s
printf "\n2. Generating author mapping at $prjRoot/$authorMap\n"
cd $localRepo
hg log | grep user: | sort | uniq | sed 's/user: *//' > $prjRoot/$authorMap

printf "\n3. Review and update author mapping, if needed. See README for more details.\n"
read -p "hit any key to continue OR Ctrl-C to quit ..." -n1 -s
cd $prjRoot/
nano $authorMap

printf "\n4. Clone remote git repo\n"
read -p "hit any key to continue OR Ctrl-C to quit ..." -n1 -s
git clone $gitUrl

printf "\n5. Migrate from Hg to Git\n"
read -p "hit any key to continue OR Ctrl-C to quit ..." -n1 -s
cd $remoteRepo
git config core.ignoreCase false
bash $prjRoot/fast-export/hg-fast-export.sh -r $prjRoot/$localRepo -A $prjRoot/$authorMap
mv $prjRoot/$authorMap $prjRoot/$authorMap.bak

printf "\n6. Please confirm the stat of authors and commits\n"
read -p "hit any key to continue OR Ctrl-C to quit ..." -n1 -s
printf "\n"
git shortlog -sn

printf "\n7. Push to git repo\n"
read -p "hit any key to continue OR Ctrl-C to quit ..." -n1 -s
# git remote add origin $gitUrl
git push origin --all
printf "\nMigration is done.\n"