# hg2git

Steps to Migrate a Mercurial (Hg) Repo into a Git Repo

Purpose: migrate a hg repo to a git repo at bitbucket

Pre-requisite:
1. Python PiP
2. hg-fast-export (git clone https://github.com/frej/fast-export.git)
3. Mercurial for Python (pip install mercurial)
4. Bitbucket account and associated SSH key

Steps:
1. Log in bitbucket.org, and then create the corresponding Git repo proj1-git.
2. Open a Linux terminal, then go to the root path of all projects.
3. Run the script hg2git.sh
    $ bash hg2git/hg2git.sh alpha-team proj1
4. Wait until the script finish, then check the git repo proj1-git in bitbucket.

Notes:
1. the Hg repo to be migrated is proj1
2. local project root path is prjs/
3. Repo mapping between Hg and Git: proj1 => proj1-git
4. Folder layout
    prjs/
        |-proj1/
        |-proj1-git/
        |-fast-export/
        |-hg2git/
        |-proj1_authors
        |-proj1_authors.bak
5. When run without any argument, hg2git will output the help message

